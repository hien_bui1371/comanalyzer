import sys
import os

cwd = sys.path[0]
sys.path.insert(0,cwd)

import ucanlintools.LUC
import ucanlintools.LDF_parser
from ucanlintools.LDF_parser import parseLDF


from pynput import keyboard
import time

import serial.tools.list_ports
ports = serial.tools.list_ports.comports()
for port, desc, hwid in sorted(ports):
    if 'USB VID:PID=0483:5740' in hwid:
        print("Detected 'Usb2LinConverter' device at port {}".format(port))
        break
else:
    sys.exit()

""" LIN Master simulation, 2 buttons slave + backlight setup. + LDF """

ldf = parseLDF(f"{cwd}/example_LDF.ldf")
master_frame = ldf.get_message_by_name('Backlight')
slave_frame = ldf.get_message_by_name('Buttons_Status')

BacklightLevel = 1

curr_baudrate = 1300

def rx_new_data(f):
    global slave_frame
    if (f.id == slave_frame.id):
        slave_frame.decode(f.data)
        print ("LIN> " + slave_frame.diff_str())

lin = ucanlintools.LUC(port)

lin.set_new_frame_rx_handler(rx_new_data)

lin.openAsMaster() 

lin.addReceptionFrameToTable(slave_frame.id,slave_frame.len)
lin.addTransmitFrameToTable(master_frame.id,b'00')

lin.enable()

def DataToFrame():
    global BacklightLevel
    aa = master_frame.encode({'backlight_level': BacklightLevel})
    lin.addTransmitFrameToTable(master_frame.id,aa)
    print(aa)

def on_release(key):
    global BacklightLevel
    global curr_baudrate
    try:
        if key.char == 'e':
            if (BacklightLevel < 0x0f):
                BacklightLevel = BacklightLevel + 1
                DataToFrame() 
        if key.char == 'd':
            if (BacklightLevel > 0):
                BacklightLevel = BacklightLevel - 1
                DataToFrame()
        if key.char == 'a':
            if curr_baudrate < 20000:
                curr_baudrate = curr_baudrate + 10
                lin.setBaudrate(curr_baudrate)
        if key.char == 's':
            if curr_baudrate > 1000:
                curr_baudrate = curr_baudrate - 10
                lin.setBaudrate(curr_baudrate)
        
    except AttributeError:
        if key == keyboard.Key.esc:
            # Stop listener
            return False
        # print("special key")


with keyboard.Listener(
        on_release=on_release) as listener:
    listener.join()
    print("SIM END")
    lin.disable()
    del lin
    print("Deinit done")
    


