
import serial
import sys

import serial.tools.list_ports
ports = serial.tools.list_ports.comports()

for port, desc, hwid in sorted(ports):
    print("{}: {} [{}]".format(port, desc, hwid))

try:
    ser = serial.Serial("COM9",baudrate=11200, timeout=1)

except serial.SerialException as e:
    #There is no new data from serial port
    print (str(e))
    sys.exit(1)
except TypeError as e:
    print (str(e))
    ser.port.close()
    sys.exit(1)