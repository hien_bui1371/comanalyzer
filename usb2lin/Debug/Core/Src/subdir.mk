################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/lin_slcan.c \
../Core/Src/main.c \
../Core/Src/open_lin_data_layer.c \
../Core/Src/open_lin_hw_st32.c \
../Core/Src/open_lin_master_data_layer.c \
../Core/Src/open_lin_network_layer.c \
../Core/Src/open_lin_slave_data_layer.c \
../Core/Src/open_lin_transport_layer.c \
../Core/Src/slcan.c \
../Core/Src/stm32f4xx_hal_msp.c \
../Core/Src/stm32f4xx_it.c \
../Core/Src/syscalls.c \
../Core/Src/sysmem.c \
../Core/Src/system_stm32f4xx.c 

OBJS += \
./Core/Src/lin_slcan.o \
./Core/Src/main.o \
./Core/Src/open_lin_data_layer.o \
./Core/Src/open_lin_hw_st32.o \
./Core/Src/open_lin_master_data_layer.o \
./Core/Src/open_lin_network_layer.o \
./Core/Src/open_lin_slave_data_layer.o \
./Core/Src/open_lin_transport_layer.o \
./Core/Src/slcan.o \
./Core/Src/stm32f4xx_hal_msp.o \
./Core/Src/stm32f4xx_it.o \
./Core/Src/syscalls.o \
./Core/Src/sysmem.o \
./Core/Src/system_stm32f4xx.o 

C_DEPS += \
./Core/Src/lin_slcan.d \
./Core/Src/main.d \
./Core/Src/open_lin_data_layer.d \
./Core/Src/open_lin_hw_st32.d \
./Core/Src/open_lin_master_data_layer.d \
./Core/Src/open_lin_network_layer.d \
./Core/Src/open_lin_slave_data_layer.d \
./Core/Src/open_lin_transport_layer.d \
./Core/Src/slcan.d \
./Core/Src/stm32f4xx_hal_msp.d \
./Core/Src/stm32f4xx_it.d \
./Core/Src/syscalls.d \
./Core/Src/sysmem.d \
./Core/Src/system_stm32f4xx.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/lin_slcan.o: ../Core/Src/lin_slcan.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/lin_slcan.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/main.o: ../Core/Src/main.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/main.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/open_lin_data_layer.o: ../Core/Src/open_lin_data_layer.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/open_lin_data_layer.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/open_lin_hw_st32.o: ../Core/Src/open_lin_hw_st32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/open_lin_hw_st32.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/open_lin_master_data_layer.o: ../Core/Src/open_lin_master_data_layer.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/open_lin_master_data_layer.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/open_lin_network_layer.o: ../Core/Src/open_lin_network_layer.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/open_lin_network_layer.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/open_lin_slave_data_layer.o: ../Core/Src/open_lin_slave_data_layer.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/open_lin_slave_data_layer.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/open_lin_transport_layer.o: ../Core/Src/open_lin_transport_layer.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/open_lin_transport_layer.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/slcan.o: ../Core/Src/slcan.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/slcan.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/stm32f4xx_hal_msp.o: ../Core/Src/stm32f4xx_hal_msp.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/stm32f4xx_hal_msp.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/stm32f4xx_it.o: ../Core/Src/stm32f4xx_it.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/stm32f4xx_it.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/syscalls.o: ../Core/Src/syscalls.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/syscalls.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/sysmem.o: ../Core/Src/sysmem.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/sysmem.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/system_stm32f4xx.o: ../Core/Src/system_stm32f4xx.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -c -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../USB_DEVICE/Target -I../Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../USB_DEVICE/App -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Middlewares/ST/STM32_USB_Device_Library/Core/Inc -IC:/Users/Thehi/STM32Cube/Repository/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/system_stm32f4xx.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

